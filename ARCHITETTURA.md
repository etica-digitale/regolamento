# Architettura del progetto

## Rami

Il progetto si suddivide in due rami:
1. `dev`: branch di sviluppo ed introduzione di modifiche;
2. `master`: branch dove le modifiche vengono implementate.

## Struttura delle cartelle
```
.
├── ARCHITETTURA.md -> Questo file
├── .gitignore -> Elenco dei files che git deve ignorare
├── .gitlab-ci.yml -> File per la creazione delle GitLab Pages
├── .gitmodules -> Sottomoduli di git
├── config.yaml -> File di configurazione del sito
├── content -> Cartella dei contenuti del sito
│   ├── index.md -> Testo del regolamento
├── ISTRUZIONI.md -> Istruzioni per la collaborazione
├── layouts -> Cartella dei layout html
│   ├── index.html -> Pagina principale del sito
│   └── partials
│       └── footer.html -> Piè di pagina modificato del sito
├── LICENSE -> Licenza
├── README.md -> Pagina iniziale su GitLab
├── static -> Contiene favicon e loghi del sito
└── themes -> Cartella dei temi
    └── indigo -> Sottomodulo del tema utilizzato dal sito
```

