---
Title: Regolamento
---

Benvenutɜ nel gruppo Telegram di “Etica Digitale”.  
Lɜ amministratorɜ sono a disposizione di tutti, ma ricordate che sono volontariɜ ed hanno anche loro una vita privata e dei tempi limitati.  

Come ogni gruppo, anche questo ha il proprio regolamento al quale vi chiediamo di attenervi quanto più possibile.  

1. **Rispetto e cordialità sempre**  
Lɜ utenti di Etica Digitale sono svariatɜ, ciascunə coi propri ideali, opinioni e conoscenze, per cui potresti incontrare persone a un livello più alto del tuo come a un livello più basso. Il gruppo vuole essere INCLUSIVO e mai esclusivo, ragion per cui sii paziente e cerca nel tuo piccolo di aiutare e costruire, anziché offendere o [trollare](https://it.wikipedia.org/wiki/Troll_(Internet)).
Sempre in quest'ottica, lɜ utenti son pregati di scrivere un sunto brevissimo riguardo un articolo quando lo si condivide. Ricordate inoltre che molti siti di giornalismo sono dietro un abbonamento, per cui considerate di copiarlo su [PrivateBin](https://privatebin.net/), seppur momentaneamente. Pretendere che lɜ utenti accettino di pagare un canone è maleducato e problematico, sopratutto quando è un rischio per la privacy più che per il portafogli.

2. **Niente pubblicità né messaggi a cascata**  
Etica Digitale è un luogo di discussione, non siamo interessatɜ a offerte di prodotti e servizi se non sono strettamente correlati all'argomento (es. ci può interessare se avete sviluppato un'applicazione libera, ma la pubblicità di un antivirus proprietario risulterà in una punizione istantanea). <br> 
Per evitare inoltre di inondare lo staff e lɜ utenti di notifiche ma anche di seppellire discussioni, cercate di scrivere messaggi quanto più lunghi possibile per quelle che sono le vostre possibilità, evitando di generare fastidiose cascate di piccoli messaggi.

3. **Tecnicismi e discussioni lunghe**  
Sebbene questo gruppo sia nato per la discussione, la quantità di utenti e la complessità di alcuni temi ci hanno portati ad aprire una bacheca su [Lemmy](https://lemmy.ml/c/eticadigitale@feddit.it). Ogni qualvolta un discorso inizia ad allungarsi o a cadere in tecnicismi (spesso è inevitabile, lo capiamo), siete pregati di creare una discussione su Lemmy e continuarla lì, avvisando sul gruppo ed includendo il link.

4. **Evitare di andare fuori tema**  
Parliamo dell'uso e abuso di tecnologie, di software libero e di alternative, di intelligenza artificiale, di trasformazione digitale della società, di diritto alla riparazione, di etica dei videogiochi e tutti i temi collegati (di cui trovate una lista [qui](https://eticadigitale.org/su-di-noi/)).
Il gruppo non è politicizzato e non lo vuole diventare, per cui evitate discorsi con sfondi politici o propagandistici che non siano strettamente correlati agli argomenti trattati (es. può interessare parlare di net neutrality o di come possano esser stati hackerati i computer per il voto durante delle elezioni, ma scrivere “Salvini razzista” o di come tutto si risolverebbe con la completa automazione risulterà in un ban istantaneo).  

5. **Evitare attività illegali**  
Parlare di ingegneria inversa è contemplato; condividere materiale e pratiche illegali (pirateria, diffusione di informazioni personali, condivisione di materiale altrui senza permesso, ecc) tanto nel gruppo come in privato è severamente vietato. Vi ricordiamo che queste pratiche sono a tutti gli effetti dei crimini e in quanto tali sono legalmente perseguibili.  

6. **Non importunare le persone in privato**  
Se qualcuno non vuole dibattere e ha chiarito esplicitamente la sua posizione, non insistete in privato. È un comportamento irrispettoso, assillante e se portato agli estremi è passabile di denuncia.  
<br>  

Nel caso di infrazione di tali regole, a seconda della gravità lo staff si prenderà il privilegio di avvisare o bandire l'utente. Per qualsiasi domanda, informazione o segnalazione, scrivete pure all’amministrazione in privato.  
