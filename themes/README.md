# Regolamento

> ! Questo è il backend del progetto, fatto per chi vuole contribuire alla sua stesura. La versione ufficiale (e facilmente consultabile) si trova invece presso https://regolamento.eticadigitale.org

Semplicemente questo è il sito del regolamento
  
### Contatti

[Sito](https://eticadigitale.org/)  
[Canale Telegram](https://t.me/eticadigitalechannel)  
[Gruppo Telegram](https://t.me/EticaDigitale)  
mail: etica.digitale@mailfence.com
