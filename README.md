# Regolamento

> ! Questo è il backend del progetto, fatto per chi vuole contribuire alla sua stesura. La versione ufficiale (e facilmente consultabile) si trova invece presso https://regolamento.eticadigitale.org

Si ricorda inoltre che il progetto è sotto licenza libera, ovvero chiunque è libero di contribuire alla sua stesura, crearne una propria versione e/o ridistribuirla.  
Per ulteriori domande, consultate le [FAQ](https://regolamento.eticadigitale.org/faq/) (domande più frequenti).

### Contatti

[Sito](https://eticadigitale.org/)  
[Canale Telegram](https://t.me/eticadigitalechannel)  
[Gruppo Telegram](https://t.me/EticaDigitale)  
mail: etica.digitale@mailfence.com
